//
//  ISSPTests.swift
//  ISSPTests
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import XCTest
@testable import ISSP

class ISSPTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUnixDateConversion() {
        let riseTime: Int = 1517522229
        let expectedDateStr: String = "2018-02-01 21:57"
        let dateStr:String = riseTime.unixTimeStampToDateStr()
        
        XCTAssertEqual(dateStr, expectedDateStr)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
