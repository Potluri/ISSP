//
//  ISSPServices.swift
//  ISSP
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let kBaseURL: String = "http://api.open-notify.org"

enum Result<T> {
    case Success(T)
    case Failure(Error)
}

protocol Gettable {
    associatedtype Data
    
    func getPasses(lat: Double, long: Double, completionHandler: @escaping (Result<Data>) -> Void)
}

struct ISSPServices: Gettable {
    
    func getPasses(lat: Double, long: Double, completionHandler: @escaping (Result<[Passes]>) -> Void) {
        let urlStr = String(format: "%@/iss-pass.json?lat=%f&lon=%f", kBaseURL, lat, long)
        guard let url = URL(string: urlStr) else { return }
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default)
            .responseJSON(completionHandler: { (responseData) in
                if let error = responseData.error, responseData.result.value == nil {
                    completionHandler(Result.Failure(error))
                }
                guard let json = responseData.result.value else {
                    ISSPAlertView.passAlertView.show()
                    return
                }
                
                let response = ISSPassesResponse(object: json)
                if let passList = response.passes {
                    completionHandler(Result.Success(passList))
                }
            })
    }
}
