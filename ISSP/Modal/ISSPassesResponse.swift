//
//  BaseClass.swift
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ISSPassesResponse {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let response = "response"
    static let message = "message"
    static let request = "request"
  }

  // MARK: Properties
  public var passes: [Passes]?
  public var message: String?
  public var request: Request?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    if let items = json[SerializationKeys.response].array { passes = items.map { Passes(json: $0) } }
    message = json[SerializationKeys.message].string
    request = Request(json: json[SerializationKeys.request])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = passes { dictionary[SerializationKeys.response] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = request { dictionary[SerializationKeys.request] = value.dictionaryRepresentation() }
    return dictionary
  }

}
