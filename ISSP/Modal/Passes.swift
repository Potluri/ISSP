//
//  Response.swift
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Passes {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let duration = "duration"
        static let risetime = "risetime"
    }
    
    // MARK: Properties
    public var duration: Int?
    public var risetime: Int?
    
    public var riseDateString: String {
        get {
            return risetime?.unixTimeStampToDateStr() ?? ""
        }
    }
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        duration = json[SerializationKeys.duration].int
        risetime = json[SerializationKeys.risetime].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = risetime { dictionary[SerializationKeys.risetime] = value }
        return dictionary
    }
    
}
