//
//  Extensions.swift
//  ISSP
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation

extension Int {
    func unixTimeStampToDateStr() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.string(from: date)
    }
}
