////
//  ISSPUserUtils.swift
//  ISSP
//
//  Created by Potluri on 01/01/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit

class ISSPAlertView {
    static let noInternetAlertView: UIAlertView = UIAlertView(title: "", message: "Unable to connect to Internet!", delegate: nil, cancelButtonTitle: "Ok")
    static let passAlertView: UIAlertView = UIAlertView(title: "Error", message: "Unable to fetch passes!", delegate: nil, cancelButtonTitle: "Ok")
}

class ISSPUserUtils: NSObject {

    class func isNetworkReachable() -> Bool{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.reach.connection == .none {
            if !ISSPAlertView.noInternetAlertView.isVisible {
                ISSPAlertView.noInternetAlertView.show()
            }
        }
        return (appDelegate.reach.connection != .none)
    }
    
}
