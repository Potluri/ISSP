//
//  MasterViewController.swift
//  ISSP
//
//  Created by Potluri on 01/02/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UITableViewController {
    let locationManager = CLLocationManager()
    
    // setting default location
    var currentLocation = CLLocation(latitude: 37.758765, longitude: -122.448246)
    
    var passes = [Passes]() {
        didSet {
            tableView.reloadData()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.refreshControl?.addTarget(self, action: #selector(fetchISSPasses), for: .valueChanged)

        self.fetchISSPasses()

        locationManager.requestWhenInUseAuthorization()
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let pass = passes[indexPath.row]
        if let duration = pass.duration {
            cell.textLabel?.text = String(format: "RiseTime: %@", pass.riseDateString)
            cell.detailTextLabel?.text = String(format: "Duration: %d", duration)
        } else {
            cell.textLabel?.text = "RiseTime:"
            cell.textLabel?.text = "Duration:"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func getPasses<Service: Gettable>(fromService service: Service) where Service.Data == [Passes] {
        self.refreshControl?.beginRefreshing()
        service.getPasses(lat: currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude){ [weak self] result in
            self?.refreshControl?.endRefreshing()
            switch result {
            case .Success(let food):
                self?.passes = food
            case .Failure(let error):
                ISSPAlertView.passAlertView.show()
            }
        }
    }
    
    @objc func fetchISSPasses() {
        getPasses(fromService: ISSPServices())
    }
    
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            self.fetchISSPasses()
        }
    }
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Location Access Disabled",
                                                message: "Unable to determine location to show Passes",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
